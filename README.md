# AppArmor Loader

## Overview

Helm chart based on the following repository: https://github.com/kubernetes/kubernetes/tree/master/test/images/apparmor-loader.

This chart also supports [Pod Security
Policies](https://kubernetes.io/docs/concepts/policy/pod-security-policy)
deployment so you could activate loaded profiles across multiple pods.

At the moment, there is a [limitation](https://github.com/kubernetes/kubernetes/tree/master/test/images/apparmor-loader#limitations) on updating and deleting profiles.

## Profiles

Profiles can be added by overwriting `profiles` as the following:
```
profiles:
  profile-one: |-
    profile profile-one {
      file,
    }
  profile-two: |-
    profile profile-two {
      umount,
    }
```

## PodSecurityPolicy

PodSecurityPolicies can be added by setting `securityPolicies` as the following:
```
securityPolicies:
  example:
    defaultProfile: profile-one
    allowedProfiles:
    - profile-one
    - profile-two
    spec:
      privileged: false
      seLinux:
        rule: RunAsAny
      supplementalGroups:
        rule: RunAsAny
      runAsUser:
        rule: RunAsAny
      fsGroup:
        rule: RunAsAny
      volumes:
        - '*'
```

## Further information

More information on the AppArmor profile language can be found in: 
 - [Quick guide](https://gitlab.com/apparmor/apparmor/-/wikis/QuickProfileLanguage)
 - [Full reference](https://gitlab.com/apparmor/apparmor/-/wikis/AppArmor_Core_Policy_Reference)
 - [Policy layout](https://gitlab.com/apparmor/apparmor/-/wikis/Policy_Layout)
